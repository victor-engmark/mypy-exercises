# mypy exercises

[![pipeline status](https://gitlab.com/engmark/mypy-exercises/badges/main/pipeline.svg)](https://gitlab.com/engmark/mypy-exercises/-/commits/main)
[![coverage report](https://gitlab.com/engmark/mypy-exercises/badges/main/coverage.svg)](https://gitlab.com/engmark/mypy-exercises/-/commits/main)
[![Checked with mypy](https://mypy-lang.org/static/mypy_badge.svg)](https://mypy-lang.org/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Python: 3.9-3.13](https://img.shields.io/badge/Python-3.9--3.13-blue)](https://www.python.org/)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![shellcheck: passing](https://img.shields.io/badge/shellcheck-passing-brightgreen)](https://www.shellcheck.net/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Exercises to learn about Python type hinting using mypy.

## Table of contents

[[_TOC_]]

## What are Python type hints?

-  _Optional._ You can use as many or as few type annotations as you want, and
   can introduce them gradually.
-  _New syntax._ `name: str = "Jane"` declares the variable/field/parameter
   `name` to have the _builtin string type_ and the value "Jane".
-  _Not enforced at runtime._

## What is mypy

-  _Static type checker._ That is, it will check your code for any wrong or
   missing annotations, such as trying to pass a string where a number is
   expected.
-  _Official._ There are other type checkers, but this one is likely to be the
   best supported long-term.

## Why

See the files in the [why](src/why) directory. Run `mypy src/why` to show the
relevant error messages.

## Setup

### Website

If you don't want to install anything you can use
[mypy Playground](https://mypy-play.net/?flags=strict&mypy=1.15.0&python=3.9) to
verify your type annotations in a browser.

### [Nix](https://nixos.org/download.html)

Run `nix develop` whenever you are in this directory to enable the development
environment.

### Other

Prerequisites:

-  [coverage](https://coverage.readthedocs.io/en/latest/install.html#install)
-  [gitlint](https://jorisroovers.com/gitlint/latest/installation/)
-  [mutmut](https://mutmut.readthedocs.io/en/latest/#install-and-run)
-  [Python 3.9+](https://www.python.org/downloads/)
-  [pre-commit](https://pre-commit.com/#install)
-  [Ruff](https://docs.astral.sh/ruff/installation/)
-  [ShellCheck](https://github.com/koalaman/shellcheck#user-content-installing)
-  [uv](https://docs.astral.sh/uv/getting-started/installation/)

```shell
uv sync
```

## Running exercises

Open `exercise.py` and `test_all.py` but _not `solution.py`_ in the relevant
`src/…` subdirectory in your favourite IDE, and modify `exercise.py` until
`mypy DIRECTORY` (replacing `DIRECTORY` with the `src/…` directory path) passes.
The resulting code should also pass
`coverage run --module pytest && coverage report`, otherwise your changes have
altered the functionality! 🙂

Once you have a working solution, try comparing it to the one in `solution.py`.
In some cases there are several valid solutions.

## Exercises in order of increasing difficulty

1. [Simple built-in types](src/builtin)
1. [Instance types](src/instance)
1. [The null type](src/none)
1. [Arbitrary type](src/any)
1. [Joining types](src/union)
1. [Always failing methods](src/no_return)
1. [Object type](src/object_type)
1. [Generic types](src/generic)
1. [Optionals](src/optional)
1. [Class types](src/class_)
1. [Functions](src/callable)
1. [Annotated variables](src/annotated)
1. [JSON type](src/json_)
1. [Type checking switch](src/type_checking)
1. [Generators](src/generator)
1. [User-defined protocols](src/protocol)

## Strategies for introducing type hints

All the `solution.py` files pass
[strict mypy validation](https://mypy.readthedocs.io/en/stable/command_line.html#cmdoption-mypy-strict),
which is much stricter than the default rules. If you're starting from an
existing non-trivial project it might be easiest to start without this flag,
then introduce the flags implied by `--strict` one at a time, before replacing
all of them with just `--strict`. That way you can avoid overwhelming yourself
with new information.

If you're starting a new project I'd recommend using the
[Toitū Te Whenua LINZ Python template repo](https://github.com/linz/template-python-hello-world/),
which is already set up with lots of automated quality assurance, including, of
course, mypy.

If you run into errors you don't understand yet, you can
[silence an error](https://mypy.readthedocs.io/en/stable/error_codes.html#silencing-errors-based-on-error-codes)
until you have time to deal with it.

## Debugging types

You can use
[`reveal_type(EXPR)`](https://mypy.readthedocs.io/en/stable/common_issues.html#reveal-type)
to tell `mypy` to print the inferred type of an expression. This function is
_only_ available when running `mypy` - it's not part of Python and you don't
need to import it.

## Configuring mypy

See [reference docs](https://mypy.readthedocs.io/en/stable/config_file.html). I
would recommend putting the configuration in [pyproject.toml](pyproject.toml).
This project uses a separate [configuration file](mypy-ci.ini) when running
automated tests, since the exercise code _should_ have some errors in it.

## Contributing

Contributions are very welcome! If you find something which could be improved,
feel free to
[file an issue](https://gitlab.com/engmark/mypy-exercises/-/issues/new). If you
have time to contribute content, all the better. Please see the next section.

## Development

The following will help with making sure you only commit valid content, by
checking adherence to the [project linters](.pre-commit-config.yaml):

```shell
pre-commit install --hook-type=commit-msg --hook-type=pre-commit --overwrite
```

Make sure that all the [pipeline checks](.gitlab-ci.yml) pass, or the merge
request will not be accepted.

## Copyright

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")

This content is copyright © 2021 Victor Engmark, and licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
A copy of the license is provided in the [LICENSE](LICENSE) file.

## Acknowledgements

A big thanks to my employer,
[Toitū Te Whenua Land Information New Zealand](https://www.linz.govt.nz/), for
allowing me to work on this.
