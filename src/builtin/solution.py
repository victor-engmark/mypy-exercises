def format_error_message(severity: str, code: int, message: str) -> str:
    return f"{severity} {code}: '{message}'"
