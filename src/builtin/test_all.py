from pytest_subtests import SubTests

from .exercise import format_error_message as exercise
from .solution import format_error_message as solution

SEVERITY = "ERROR"
CODE = 42
MESSAGE = "some message"
EXPECTED = "ERROR 42: 'some message'"


def test_should_return_formatted_message(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            assert function(SEVERITY, CODE, MESSAGE) == EXPECTED
