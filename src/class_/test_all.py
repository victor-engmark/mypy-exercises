from unittest.mock import patch

from pytest_subtests import SubTests

from ..generators import any_string
from . import exercise, solution

ARBITRARY_IP = "192.168.0.1"


def test_model_base_does_not_have_ip(subtests: SubTests) -> None:
    for module in (exercise, solution):
        with subtests.test(msg=module.__name__):
            assert not hasattr(module.ModelBase(any_string()), "ip")


def test_should_get_public_ip(subtests: SubTests) -> None:
    hostname = any_string()
    for module in (exercise, solution):
        with (
            subtests.test(msg=module.__name__),
            patch(f"{module.__name__}.gethostname") as gethostname_mock,
        ):
            gethostname_mock.return_value = hostname

            model = module.model_with_meta()

            assert model.hostname == hostname


def test_should_initialise_model_with_given_ip(subtests: SubTests) -> None:
    for module in (exercise, solution):
        with subtests.test(msg=module.__name__):
            model = module.model_with_meta(hostname=ARBITRARY_IP)

            assert model.hostname == ARBITRARY_IP
