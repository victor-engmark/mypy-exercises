"""Docs:

* typing library
<https://docs.python.org/3/library/stdtypes.html#classes-and-class-instances>
* mypy
<https://mypy.readthedocs.io/en/stable/kinds_of_types.html#the-type-of-class-objects>
"""

from dataclasses import dataclass
from socket import gethostname


@dataclass  # pragma: no mutate
class ModelBase:
    hostname: str


def model_with_meta(hostname=None):
    actual_hostname = hostname if hostname else gethostname()

    @dataclass  # pragma: no mutate
    class Model(ModelBase):
        hostname = actual_hostname

    return Model
