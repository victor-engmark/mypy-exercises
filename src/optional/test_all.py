from datetime import datetime

from pytest_subtests import SubTests

from .exercise import within_datetime_range as exercise
from .solution import within_datetime_range as solution

BEFORE_START_DATETIME = datetime(1, 1, 1)
START_DATETIME = datetime(2, 1, 1)
INTERMEDIATE_DATETIME = datetime(3, 1, 1)
END_DATETIME = datetime(4, 1, 1)
AFTER_END_DATETIME = datetime(5, 1, 1)


def test_should_treat_intermediate_datetime_as_within_closed_range(
    subtests: SubTests,
) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=f"{function.__module__} {BEFORE_START_DATETIME}"):
            assert not function(START_DATETIME, BEFORE_START_DATETIME, END_DATETIME)

        with subtests.test(msg=f"{function.__module__} {INTERMEDIATE_DATETIME}"):
            assert function(START_DATETIME, INTERMEDIATE_DATETIME, END_DATETIME)

        with subtests.test(msg=f"{function.__module__} {AFTER_END_DATETIME}"):
            assert not function(START_DATETIME, AFTER_END_DATETIME, END_DATETIME)


def test_should_treat_late_datetimes_as_within_open_ended_range(
    subtests: SubTests,
) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=f"{function.__module__} {BEFORE_START_DATETIME}"):
            assert not function(START_DATETIME, BEFORE_START_DATETIME, None)

        with subtests.test(msg=f"{function.__module__} {INTERMEDIATE_DATETIME}"):
            assert function(START_DATETIME, INTERMEDIATE_DATETIME, None)

        with subtests.test(msg=f"{function.__module__} {AFTER_END_DATETIME}"):
            assert function(START_DATETIME, AFTER_END_DATETIME, None)


def test_should_treat_early_datetimes_as_within_open_started_range(
    subtests: SubTests,
) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=f"{function.__module__} {BEFORE_START_DATETIME}"):
            assert function(None, BEFORE_START_DATETIME, END_DATETIME)

        with subtests.test(msg=f"{function.__module__} {INTERMEDIATE_DATETIME}"):
            assert function(None, INTERMEDIATE_DATETIME, END_DATETIME)

        with subtests.test(msg=f"{function.__module__} {AFTER_END_DATETIME}"):
            assert not function(None, AFTER_END_DATETIME, END_DATETIME)


def test_should_treat_all_datetimes_as_within_open_range(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=f"{function.__module__} {BEFORE_START_DATETIME}"):
            assert function(None, BEFORE_START_DATETIME, None)

        with subtests.test(msg=f"{function.__module__} {INTERMEDIATE_DATETIME}"):
            assert function(None, INTERMEDIATE_DATETIME, None)

        with subtests.test(msg=f"{function.__module__} {AFTER_END_DATETIME}"):
            assert function(None, AFTER_END_DATETIME, None)


def test_should_treat_datetime_at_start_as_within_range(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            assert function(START_DATETIME, START_DATETIME, None)


def test_should_treat_datetime_at_end_as_outside_range(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            assert not function(START_DATETIME, END_DATETIME, END_DATETIME)
