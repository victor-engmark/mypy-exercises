from datetime import datetime
from typing import Optional


def within_datetime_range(
    start: Optional[datetime], target: datetime, end: Optional[datetime]
) -> bool:
    return (start is None or start <= target) and (end is None or target < end)
