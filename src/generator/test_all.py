from pytest_subtests import SubTests

from .exercise import fibonacci as exercise
from .solution import fibonacci as solution


def test_should_return_fibonacci_sequence(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            generator = function(0, 1)
            assert [next(generator) for _ in range(3)] == [0, 1, 1]
