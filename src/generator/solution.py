from typing import Iterator


def fibonacci(first: int, second: int) -> Iterator[int]:
    yield first

    while True:
        yield second
        first, second = second, first + second
