"""Docs:

* typing library <https://docs.python.org/3/library/typing.html#typing.Generator>
* mypy <https://mypy.readthedocs.io/en/latest/kinds_of_types.html#generators>"""


def fibonacci(first, second):
    yield first

    while True:
        yield second
        first, second = second, first + second
