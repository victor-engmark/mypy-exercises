"""Unions with completely different types are a code smell, especially if there are lots
of them."""

from typing import Mapping, Union

from .name_reuse import Person


def name(person: Union[Mapping[str, str], Person, str]) -> str:
    if isinstance(person, Person):
        # Person
        return person.name

    if isinstance(person, str):
        # str
        return person

    # Mapping
    return person["name"]
