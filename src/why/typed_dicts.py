"""Sometimes it makes sense to pass ``dict``s around as arguments. If they have a know
structure that can also be type hinted.

``TypedDict`` instances are ``dict``s, which means they are drop-in replacements.

See test_typed_dicts.py for a use example."""

from datetime import datetime
from typing import List, Optional

from typing_extensions import Literal, TypedDict


class SpatialExtent(TypedDict):
    bbox: List[List[int]]


class TemporalExtent(TypedDict):
    interval: List[List[Optional[datetime]]]


class Extent(TypedDict):
    spatial: SpatialExtent
    temporal: TemporalExtent


class Link(TypedDict):
    href: str


class StacCollection(TypedDict, total=False):  # pragma: no mutate
    description: str
    extent: Extent
    id: str
    license: str
    links: List[Link]
    version: str
    type: Literal["collection"]
