from typing import NoReturn


def chain_exceptions(inner: Exception, outer: Exception) -> NoReturn:
    raise outer from inner
