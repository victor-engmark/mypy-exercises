import operator
from functools import reduce
from typing import List


def product(values: List[int]) -> int:
    return reduce(operator.mul, values, 1)
