"""Docs:

* mypy <https://mypy.readthedocs.io/en/latest/builtin_types.html#generic-types>"""

import operator
from functools import reduce


def product(values):
    return reduce(operator.mul, values, 1)
