"""Hints:

* ``Any`` should rarely be necessary in production code - most of the time you can at
least specify ``object`` or a ``Union`` of possible types
* ``Any`` *disables* any further checking of that value

Docs:

* mypy <https://mypy.readthedocs.io/en/latest/builtin_types.html#any-type>"""


def commutative_equals(first, second):
    return first == second and second == first
