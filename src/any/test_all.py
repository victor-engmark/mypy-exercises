from pytest_subtests import SubTests

from ..generators import any_integer, any_string
from .exercise import commutative_equals as exercise
from .solution import commutative_equals as solution

SIMPLE_VALUES = [any_integer(), any_string()]


class LeftSideOnly:
    def __init__(self, is_left: bool):
        self.is_left = is_left

    def __eq__(self, other: object) -> bool:
        return self.is_left

    def __hash__(self) -> int:
        return super().__hash__()  # pragma: no cover


def test_should_treat_simple_value_equals_as_commutative(subtests: SubTests) -> None:
    for function in (exercise, solution):
        for value in SIMPLE_VALUES:
            with subtests.test(msg=f"{function.__module__} {value}"):
                assert function(value, value)


def test_should_treat_broken_equals_as_non_commutative(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with subtests.test(msg=function.__module__):
            assert not function(LeftSideOnly(True), LeftSideOnly(False))
