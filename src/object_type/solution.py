def is_equality_method_sane(reference: object, other: object) -> bool:
    return reference == reference and not reference == other  # noqa: PLR0124
