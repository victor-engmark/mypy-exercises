"""Caveat: This is more of a toy example than the others, because most production code
should not have to deal with completely generic objects.

Hints:

* ``Any`` disables any further type checking, while ``object`` ensures that type
checkers complain about any non-generic interactions like ``reference.foo()``
<https://mypy.readthedocs.io/en/latest/dynamic_typing.html#any-vs-object>.

Docs:

* data model <https://docs.python.org/3/reference/datamodel.html#objects>"""


def is_equality_method_sane(reference, other):
    return reference == reference and not reference == other  # noqa: PLR0124
