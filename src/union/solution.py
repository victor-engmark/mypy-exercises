"""See also ``../json_``."""

from typing import Dict, Union

from .exercise import BODY_KEY, STATUS_KEY


def create_response(code: Union[int, str], body: str) -> Dict[str, Union[int, str]]:
    return {STATUS_KEY: code, BODY_KEY: body}
