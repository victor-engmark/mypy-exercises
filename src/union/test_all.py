from typing import List, Union

from pytest_subtests import SubTests

from .exercise import create_response as exercise
from .solution import create_response as solution


def test_should_create_response(subtests: SubTests) -> None:
    body = "Somebody"
    codes: List[Union[int, str]] = [1, "failure"]

    for function in (exercise, solution):
        for code in codes:
            with subtests.test(msg=f"{function.__module__}: {code}"):
                assert function(code, body) == {"status": code, "body": body}
