"""Hints:

* ``Callable`` is the function type, and it takes two arguments: the argument list and
the return type
* The ``Callable`` argument list corresponding to any number of ``Any``s is ``...``
* You can create a ``TypeVar`` to link the return type of the original and curried
functions together.

Docs:

* typing library <https://docs.python.org/3/library/typing.html#typing.Callable>
* mypy
<https://mypy.readthedocs.io/en/latest/kinds_of_types.html#callable-types-and-lambdas>
"""


def curry(original_function, *original_args, **original_kwargs):
    def curried_function(*args, **kwargs):
        return original_function(*original_args, *args, **original_kwargs, **kwargs)

    return curried_function
