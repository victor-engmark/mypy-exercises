from typing import Any, Callable, TypeVar

Result = TypeVar("Result")  # pragma: no mutate


def curry(
    original_function: Callable[..., Result],
    *original_args: Any,
    **original_kwargs: Any,
) -> Callable[..., Result]:
    def new_function(*args: Any, **kwargs: Any) -> Result:
        return original_function(*original_args, *args, **original_kwargs, **kwargs)

    return new_function
