"""How would you create a type such that it contains enough information to validate
values as instances of that type?

Hints:

* This exercise is more contrived than usual, meant to show how you can use type
annotations to create restricted variants of basic types.
* ``Annotated[<type>, <metadata>]`` is treated as ``<type>`` by mypy, letting you use
whatever you like in the metadata.
* You can have one or more ``<metadata>`` entries.
* You can get the base type and metadata from an ``Annotated`` type with
``typing.get_args``.

Docs:

* typing library <https://docs.python.org/3/library/typing.html#typing.Annotated>"""


def is_even(value):
    return value % 2 == 0


def is_positive(value):
    return value > 0


def is_even_positive_integer(value):
    return is_even(value) and is_positive(value)
