from typing import Annotated, Any, get_args


def is_even(value: int) -> bool:
    return value % 2 == 0


def is_positive(value: int) -> bool:
    return value > 0


EvenPositiveInteger = Annotated[int, is_even, is_positive]


def is_even_positive_integer(value: Any) -> bool:
    """Creating a generic ``Annotated`` type validator would be as simple as extracting
    ``EvenPositiveInteger`` as a function parameter."""
    base_type, *validators = get_args(EvenPositiveInteger)
    return isinstance(value, base_type) and all(
        validator(value) for validator in validators
    )
