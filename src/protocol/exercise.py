"""Hints:

* To use a self-referencing type you need to quote it. For example::

    class Foo:
        def bar(instance: "Foo") -> Bar:
            return Bar(instance)

* Protocols are a way to define a contract for a class, similar to an interface in other
languages
* See for example <https://hynek.me/articles/python-subclassing-redux/>

Docs:

* Python standard library <https://docs.python.org/3/library/typing.html#protocols>
* mypy <https://mypy.readthedocs.io/en/latest/protocols.html>"""


class InvertibleDict(dict):
    def inverse(self):
        new_instance = self.__class__()
        for key, value in self.items():
            new_instance[value] = key
        return new_instance
