"""Docs:

* JSON type: <https://github.com/python/typing/issues/182#issuecomment-1320974824>"""


def strict_merge_json_dicts(first, second):
    if overlapping_keys := first.keys() & second.keys():
        raise OverlappingJSONError(overlapping_keys)

    return {**first, **second}


class OverlappingJSONError(Exception):
    pass
