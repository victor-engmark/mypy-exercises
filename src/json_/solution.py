from typing import Dict, List, Union

from src.json_.exercise import OverlappingJSONError

Json = Union[
    Dict[str, "Json"], List["Json"], str, int, float, bool, None  # pragma: no mutate
]  # pragma: no mutate
JsonDict = Dict[str, Json]  # pragma: no mutate


def strict_merge_json_dicts(first: JsonDict, second: JsonDict) -> JsonDict:
    if overlapping_keys := first.keys() & second.keys():
        raise OverlappingJSONError(overlapping_keys)

    return {**first, **second}
