from logging import getLogger

from .exercise import LOG_FORMAT

LOGGER = getLogger(__name__)


def log_message(program: str, message: str) -> None:
    LOGGER.debug(LOG_FORMAT, program, message)
