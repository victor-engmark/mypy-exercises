"""Docs:

* Python standard library
<https://docs.python.org/3/library/stdtypes.html#the-null-object>
* mypy
<https://mypy.readthedocs.io/en/latest/kinds_of_types.html#optional-types-and-the-none-type>
"""

from logging import getLogger

LOGGER = getLogger(__name__)
LOG_FORMAT = "%s: %s"


def log_message(program, message):
    LOGGER.debug(LOG_FORMAT, program, message)
