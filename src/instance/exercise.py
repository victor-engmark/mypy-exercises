"""Hints:

* ``__init__`` does not need a return type if it takes at least one argument - it is
assumed to be ``None``

Docs:

* typing library
<https://docs.python.org/3/library/stdtypes.html#classes-and-class-instances>
* data model <https://docs.python.org/3/reference/datamodel.html#objects>
* mypy <https://mypy.readthedocs.io/en/stable/kinds_of_types.html#class-types>"""

from dataclasses import dataclass


@dataclass  # pragma: no mutate
class Person:
    def __init__(self, name):
        self.name = name

    def to_dict(self):
        return {"name": self.name}


def person_from_dict(value):
    return Person(value["name"])
