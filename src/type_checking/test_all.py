from pytest_subtests import SubTests

from .exercise import caller_file as exercise
from .solution import caller_file as solution


def test_should_return_caller_file_io(subtests: SubTests) -> None:
    for function in (exercise, solution):
        with (
            subtests.test(msg=function.__module__),
            function("rb") as function_file_descriptor,
            open(__file__, "rb") as self_file_descriptor,
        ):
            assert function_file_descriptor.read() == self_file_descriptor.read()
