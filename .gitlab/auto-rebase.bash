#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

gitlab_api_url=https://gitlab.com/api/v4/projects

curl_command=(curl --header "PRIVATE-TOKEN: ${GITLAB_PERSONAL_TOKEN:?}")

opened_merge_requests="$("${curl_command[@]}" "${gitlab_api_url}/${CI_PROJECT_ID:?}/merge_requests?state=opened&target_branch=${CI_DEFAULT_BRANCH:?}&wip=no")"

raw_iids="$(jq --raw-output '.[] | .iid' <<< "${opened_merge_requests}")"

readarray -t iids <<< "${raw_iids}"

for iid in "${iids[@]}"; do
    "${curl_command[@]}" --request PUT "${gitlab_api_url}/${CI_PROJECT_ID:?}/merge_requests/${iid}/rebase"
done
