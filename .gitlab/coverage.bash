#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

coverage run --module pytest --junitxml=report.xml --randomly-seed="${CI_JOB_ID:?}"

coverage report || {
    exit_code="$?"
    if (("${exit_code}" != 2)); then
        exit "${exit_code}"
    fi
}

coverage xml
