#!/bin/sh

set -o errexit -o noclobber -o nounset

pip install uv
uv sync --link-mode=copy
