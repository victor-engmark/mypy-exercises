#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

for exercise in src/*/exercise.py; do
    if mypy "${exercise}" > /dev/null; then
        printf 'No errors found in %s!\n' "${exercise}"
        exit_code=1
    fi
done

exit "${exit_code-0}"
