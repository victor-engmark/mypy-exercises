#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

mutmut run --no-progress --runner="pytest --assert=plain --exitfirst --randomly-seed=${CI_JOB_ID:?}" ${@+"$@"} || {
    exit_code="$?"
}

if [[ -e mutmut.xml ]]; then
    rm --verbose mutmut.xml
fi
mutmut junitxml > mutmut.xml

exit "${exit_code-0}"
